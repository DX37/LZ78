#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "lz78.h"
#include "misc.h"

#define DICT_SIZE 65536

int compress(char *in, char *out)
{
    FILE *input;
    input = fopen(in, "r");
    if (input == NULL)
        return -1;

    int PHRASE_SIZE = (log(DICT_SIZE)/log(2) + 0.5) + 8;
    phrase *dict = (phrase*)calloc(DICT_SIZE, sizeof(phrase));

    char **d = (char**)calloc(DICT_SIZE, sizeof(char*));
    for (int i = 0; i < DICT_SIZE; i++)
        d[i] = (char*)calloc(PHRASE_SIZE, sizeof(char));

    int len = 0;
    char ch = '\0';
    while (!feof(input))
    {
        fscanf(input, "%c", &ch);
        len++;
    }
    rewind(input);
    char *string = (char*)calloc(len, sizeof(char));
    for (int i = 0; i < len; i++)
        fscanf(input, "%c", &string[i]);
    fclose(input);

    int i, j, count = 1, k = 0;
    char *str = (char*)calloc(PHRASE_SIZE, sizeof(char));
    for (i = 0; i < len; i++)
    {
        if (count == DICT_SIZE)
        {
            free(dict);
            dict = (phrase*)calloc(DICT_SIZE, sizeof(phrase));
            clean(d, DICT_SIZE);
            d = (char**)calloc(DICT_SIZE, sizeof(char*));
            for (int i = 0; i < DICT_SIZE; i++)
                d[i] = (char*)calloc(DICT_SIZE, sizeof(char));
            count = 1;
        }
        str[k] = string[i];
        str[k+1] = '\0';
        for (j = 1; j < count; j++)
        {
            if (strcmp(str, d[j]) == 0)
            {
                k += strlen(str);
                dict[count].index = j;
                break;
            }
            k = 0;
        }
        strcpy(d[count], str);
        dict[count].data = str[strlen(str)-1];
        if (k == 0)
            count++;
    }
    count += 1;

    FILE *output;
    output = fopen(out, "wb");
    for (int i = 0; i < count; i++)
    {
        fwrite(&dict[i].index, sizeof(int), 1, output);
        fwrite(&dict[i].data, sizeof(char), 1, output);
    }
    fclose(output);

    free(str);
    free(string);
    free(dict);
    clean(d, DICT_SIZE);
    return 0;
}

int decompress(char *in, char *out)
{
    FILE *input;
    input = fopen(in, "rb");
    if (input == NULL)
        return -1;

    int PHRASE_SIZE = (log(DICT_SIZE)/log(2) + 0.5) + 8;
    phrase *dict = NULL, *tmp, phr;

    int count = 0;
    while (!feof(input))
    {
        fread(&phr.index, sizeof(int), 1, input);
        fread(&phr.data, sizeof(char), 1, input);
        count++;
        tmp = (phrase*)realloc(dict, count*sizeof(phrase));
        dict = tmp;
        dict[count-1].index = phr.index;
        dict[count-1].data = phr.data;
    }
    fclose(input);

    count -= 1;
    FILE *output;
    output = fopen(out, "w");
    int i, j;
    char *str = (char*)calloc(PHRASE_SIZE, sizeof(char));
    for (i = 0; i < count; i++)
    {
        int a = 0;
        if (dict[i].index != 0)
        {
            str[a] = dict[i].data;
            str[a+1] = '\0';
            a++;
            j = dict[i].index;
            while (j != 0)
            {
                str[a] = dict[j].data;
                str[a+1] = '\0';
                j = dict[j].index;
                a++;
            }
        }
        else
        {
            str[a] = dict[i].data;
            str[a+1] = '\0';
        }
        reverse(str);
        fprintf(output, "%s", str);
    }
    fclose(output);

    free(str);
    free(dict);
    return 0;
}

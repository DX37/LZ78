#ifndef LZ78_H
#define LZ78_H

typedef struct {
    int index;
    char data;
} phrase;

int compress(char *in, char *out);
int decompress(char *in, char *out);

#endif

#include <stdlib.h>
#include <string.h>
#include "misc.h"

void reverse(char *s)
{
    int i, j;
    for (i = 0, j = strlen(s)-1; i<j; i++, j--)
    {
        char c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void clean(char **mem, int size)
{
    for (int i = 0; i < size; i++)
        free(mem[i]);
    free(mem);
}

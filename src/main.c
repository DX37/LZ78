#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lz78.h"

void printHelp(char *progName)
{
    printf("Usage: %s <key> <input_file> <output_file>\n", progName);
    printf("=== Keys ===\n");
    printf("-c - compress text file\n");
    printf("-d - decompress text file\n");
}

int main(int argc, char *argv[])
{
    if (argc == 1)
        printHelp(argv[0]);
    else if (!strcmp(argv[1], "-c"))
    {
        if (!argv[2])
            printf("Enter input file.\n");
        else if (!argv[3])
            printf("Enter output file.\n");
        else switch (compress(argv[2], argv[3]))
        {
            case 0:
                return 0;
                break;
            case -1:
                printf("Input file not found.\n");
                break;
            default:
                return 0;
                break;
        }
    }
    else if (!strcmp(argv[1], "-d"))
    {
        if (!argv[2])
            printf("Enter input file.\n");
        else if (!argv[3])
            printf("Enter output file.\n");
        else switch (decompress(argv[2], argv[3]))
        {
            case 0:
                return 0;
                break;
            case -1:
                printf("Input file not found.\n");
                break;
            default:
                return 0;
                break;
        }
    }
    return 0;
}
